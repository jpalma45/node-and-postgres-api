import express, {json} from 'express';
import morgan from 'morgan';

//Importing routes
import projectRoutes from './routes/projects';
import taskRoutes from './routes/tasks';

const app = express();

// middleware
app.use(json());
app.use(morgan('dev'));

// use routes
app.use('/api/projects', projectRoutes);
app.use('/api/tasks', taskRoutes);

export default app;