import Task from '../models/Task';

export async function updateTask(req, res) {
    const { id } = req.params;
    const { name, done, projectid } = req.body;

    try {
        let tasks = await Task.findAll({
            attributes: ['id', 'name','done', 'projectid'],
            where: {
                id
            }
        });
        if (tasks.length > 0) {
            tasks.forEach(async task =>{
                await task.update({
                    name,
                    done,
                    projectid
                });
            });
        }
        res.status(200).json({
            message: 'Project updated successfully',
            data: tasks
        })
    } catch (error) {
        res.status(500).json({
            message: 'Something goes wrong',
            data: {}
        });    
    }
}

export async function deleteTask(req, res) {
    const { id } = req.params;
    try {
        let deleteRowCount = await Task.destroy({
            where: {
                id
            }
        });
        res.status(200).json({
            message: 'delete project successfully',
            count: deleteRowCount
        })
    } catch (error) {
        res.status(500).json({
            message: 'Something goes wrong',
            data: {}
        });    
    }
}

export async function getTaskById(req, res) {
    const { id } = req.params;
    try {
        let task = await Task.findOne({
            attributes: ['id', 'projectid', 'name', 'done'],
            order: [
                ['id', 'DESC']
            ],
            where: {
                id
            }
        });
        res.status(200).json({
            message: 'get project successfully',
            data: task
        })
    } catch (error) {
        res.json({
            message: 'Something goes wrong',
            data: {},
          });
    }
}

export async function getTask(req, res) {
  try {
    let tasks = await Task.findAll({
        attributes: ['id', 'projectid', 'name', 'done'],
        order: [
            ['id', 'DESC']
        ]
    });
    return res.status(200).json({
      message: "get projects successfully",
      data: tasks,
    });
  } catch (error) {
    console.log(error)
    res.json({
      message: "Something goes wrong",
      data: {},
    });
  }
}

export async function getTaskByProject(req, res){
    const { projectid } = req.params;
    try {
        let task = await Task.findOne({
            attributes: ['id', 'projectid', 'name', 'done'],
            order: [
                ['id', 'DESC']
            ],
            where: {
                projectid
            }
        });
        res.status(200).json({
            message: 'get project successfully',
            data: task
        })
    } catch (error) {
        res.json({
            message: 'Something goes wrong',
            data: {},
          });
    }
}

export async function createTask(req, res) {
  const { name, done, projectid } = req.body;

  try {
      let newTask = await Task.create({
          name,
          done,
          projectid
      },{
          fields: [
              'name',
              'done',
              'projectid'
          ]
      });
      if (newTask) {
          return res.status(200).json({
              message: 'Task created successfully',
              data: newTask
          });
      }
  } catch (error) {
    console.log(error)
    res.status(500).json({
        message: 'Something goes wrong',
        data: {}
    });
  }
}


