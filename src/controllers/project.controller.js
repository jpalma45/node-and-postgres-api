import Project from "../models/Project";


export async function getProjects(req, res) {
  try {
    let projects = await Project.findAll();
    return res.json({
        message: 'get projects successfully',
        data: projects,
    });
  } catch (error) {
    res.json({
      message: 'Something goes wrong',
      data: {},
    });
  }
}

export async function getProjectById(req, res) {
  const { id } = req.params;  
  try {
    let project = await Project.findOne({ 
        where: {
            id
        }
     });
    return res.status(200).json({
        message: 'get project successfully"',
      data: project,
    });
  } catch (error) {
      res.status(500).json({
          message: 'Something goes wrong',
          data: {}
      });
  }
}

export async function deleteProject(req, res) {
    const { id } = req.params;
    try {
        let deleteRowCount = await Project.destroy({
            where: {
                id
            }
        });
        res.status(200).json({
            message: 'delete project successfully',
            count: deleteRowCount
        });
    } catch (error) {
        res.status(500).json({
            message: 'Something goes wrong',
            data: {}
        });
    }
}

export async function updateProject(req, res) {
    const { id } = req.params;
    const { name, priority, description, deliverydate } = req.body;

    try {
        let projects = await Project.findAll({
            attributes: ['id', 'name', 'priority', 'description', 'deliverydate'],
            where: {
                id
            }
        });
        if (projects.length > 0) {
            projects.forEach(async project => {
                await project.update({
                    name,
                    priority,
                    description,
                    deliverydate
                    
                });
            });
        }
        res.status(200).json({
            message: 'Project updated successfully',
            data: projects
        });
    } catch (error) {
        res.status(500).json({
            message: 'Something goes wrong',
            data: {}
        });
    }
}

export async function createProject(req, res) {
  const { name, priority, description, deliverydate } = req.body;
  try {
    let newProject = await Project.create({
      name,
      priority,
      description,
      deliverydate,
    },{
        fields: [
            'name',
            'priority',
            'description',
            'deliverydate'
        ]
    });
    if (newProject) {
     return res.status(200).json({
        message: "Project created successfully",
        data: newProject,
      });
    }
  } catch (error) {
      res.status(500).json({
          message: 'Something goes wrong',
          data: {}
      });
  }
}
