import { Router } from 'express';
const router = Router();

import { createTask, getTask, 
        deleteTask, getTaskById,
        updateTask, getTaskByProject } from '../controllers/task.controller';

router.post('/', createTask);
router.get('/', getTask);
router.get('/:id', getTaskById);
router.get('/project/:projectid', getTaskByProject);
router.put('/:id', updateTask);
router.delete('/:id', deleteTask);



export default router;